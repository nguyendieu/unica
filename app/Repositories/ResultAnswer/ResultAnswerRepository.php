<?php

namespace App\Repositories\ResultAnswer;

use App\Models\ResultAnswer;
use App\Repositories\Repository;

class ResultAnswerRepository extends Repository implements ResultAnswerRepositoryInterface
{

    public function getModel()
    {
        return ResultAnswer::class;
    }

    public function getResultAnswerByResult($id)
    {
       return $this->model->with('result')->where('result_id',$id)->get();
    }
}
