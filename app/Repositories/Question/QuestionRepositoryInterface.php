<?php

namespace App\Repositories\Question;

use App\Repositories\RepositoryInterface;

interface QuestionRepositoryInterface extends RepositoryInterface
{
    public function getQuestionByCondition($keyword = null);

    public function getOptionByQuestion($id);

    public function getQuestionByTest($id);

//    public function getNameTest($id);

}

