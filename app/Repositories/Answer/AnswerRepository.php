<?php

namespace App\Repositories\Answer;


use App\Models\Answer;
use App\Models\Question;
use App\Repositories\Repository;

class AnswerRepository extends Repository implements AnswerRepositoryInterface
{
    public function getModel()
    {
        return Answer::class;
    }

    public function getAnswerByCondition($keyword = null)
    {
        return $this->model->when($keyword != null, function ($query) use ($keyword) {
            $query->where('content', 'like', '%' . $keyword . '%');
        })->with('question')->get();
    }


    public function getAnswers($idQuestion)
    {
        return $this->model->with('question')->where('question_id',$idQuestion)->get();
    }

    public function getOptionByQuestion($id) {
        return $this->model->where('question_id', $id)->get();
    }


}
