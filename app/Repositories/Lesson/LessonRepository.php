<?php

namespace App\Repositories\Lesson;


use App\Models\Course;
use App\Models\Lesson;
use App\Repositories\Repository;
use App\Services\Lessons\LessonServiceInterface;

class LessonRepository extends Repository implements LessonRepositoryInterface
{
    public function getModel()
    {
        return Lesson::class;
    }
    public function getQuestionByCondition($keyword = null)
    {
        return $this->model->when($keyword != null, function ($query) use ($keyword) {
            $query->where('content', 'like', '%' . $keyword . '%');
        })->with('course')->get();
    }

    public function getLessonsByCourse($id) {
        return $this->model
            ->with('course')
            ->where('course_id', $id)
            ->get();
    }

    public function getCourseByLearn($id) {
        return $this->model->with('course')->where('id', $id)->get();
    }

    public function getCourseID($id) {
        return $this->model->where('id', $id)->get();
    }

    public function getFirstLessonByCourse($id) {
        return $this->model->where('course_id',$id)->first();
    }
}
