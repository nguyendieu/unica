<?php

namespace App\Repositories\Role;

use App\Repositories\Repository;
use Spatie\Permission\Models\Role;

class RoleRepository extends Repository implements  RoleRepositoryInterface
{

    public function getModel()
    {
        return Role::class;
    }

    public function getNameRole()
    {
        return $this->model->pluck('name','name')->all();
    }
}
