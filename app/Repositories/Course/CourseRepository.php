<?php

namespace App\Repositories\Course;

use App\Models\Course;
use App\Repositories\Repository;



class CourseRepository extends Repository implements CourseRepositoryInterface
{

    public function getModel()
    {
        return Course::class;
    }

    public function getCourseByCondition()
    {
        return $this->model->with(['learns.user'])->get();
    }

    public function getCourseAndLessons($searchBy, $keyword) {
        return $this->model
            ->with('lessons')
            ->where($searchBy, 'like', '%' . $keyword . '%')
            ->orderBy('id', 'desc')
            ->paginate(6)
            ->appends(['search' => $keyword]);
    }

    public function getCoursePaginate($paginate) {
        return $this->model->paginate($paginate);
    }

}
