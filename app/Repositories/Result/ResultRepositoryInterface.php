<?php

namespace App\Repositories\Result;

use App\Repositories\RepositoryInterface;

interface ResultRepositoryInterface extends RepositoryInterface
{
    public function getResultStudentById($idUser);
}
