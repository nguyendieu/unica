<?php

namespace App\Repositories\Learn;

use App\Models\Learn;
use App\Repositories\Repository;



class LearnRepository extends Repository implements LearnRepositoryInterface
{

    public function getModel()
    {
        return Learn::class;
    }

    public function getStudentCount($id) {
        return $this->model->select('student_id')->where('course_id', $id)->get()->count();
    }

    public function getUserName($id) {
//        return $this->model->select('users.*')
//            ->join('users','users.id', '=', 'learns.student_id')
//            ->where('learns.course_id', $id)
//            ->get();
        return $this->model->with('user')->where('course_id', $id)->get();
    }

    public function getCourseByLearn($id) {
        return $this->model->with("course.user")->where("course_id", $id)->get();
    }

    public function getStudentCourse($id) {
        return $this->model->with('course')->where('student_id', $id)->first();
    }

    public function getStudentCourses($id) {
        return $this->model->with('course')->where('student_id', $id)->get();
    }
}
