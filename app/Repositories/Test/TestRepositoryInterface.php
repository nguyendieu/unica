<?php

namespace App\Repositories\Test;

use App\Repositories\RepositoryInterface;

interface TestRepositoryInterface extends RepositoryInterface
{
    public function getTestAndQuestion();

    public function getQuestionByTest($id);
}
