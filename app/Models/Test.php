<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use HasFactory;
    use softDeletes;
    protected $fillable = [
        'title','description'
    ];

    public function questions() {
        return $this->hasMany(Question::class, "test_id", 'id');
    }
}
