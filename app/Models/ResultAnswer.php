<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultAnswer extends Model
{
    use HasFactory;
    protected $fillable = [
        'question',
        'answer',
        'result_id'
    ];

    public function result(){
        return $this->belongsTo(Result::class,'result_id','id');
    }
}
