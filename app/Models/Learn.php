<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Learn extends Model
{
    use HasFactory;
    use softDeletes;
    protected  $dates = ['deleted_at'];
    protected $fillable = [
        'student_id','course_id','status'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'student_id','id');
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
}
