<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;
    protected $fillable = [
        'score',
        'user_id',
        'test_name'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function resultAnswers(){
        return $this->hasMany(ResultAnswer::class,'result_id','id');
    }
}
