<?php

namespace App\Services\Course;

use App\Services\ServiceInterface;

interface CourseServiceInterface extends ServiceInterface
{
    public function getCourseAndLessons($searchBy, $keyword);

    public function getCoursePaginate($paginate);
}
