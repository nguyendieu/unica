<?php

namespace App\Services\Question;

use App\Services\ServiceInterface;

interface QuestionServiceInterface extends ServiceInterface
{
    public function getQuestionByTest($id);
    public function showOptionQuestion($id);
}
