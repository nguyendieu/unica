<?php

namespace App\Services\Lessons;

use App\Repositories\Lesson\LessonRepository;

use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Services\Service;
use App\Services\ServiceInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RateLimiter\RequestRateLimiterInterface;

class LessonService extends Service implements LessonServiceInterface
{
    public  $repository;

    public function __construct(LessonRepositoryInterface $lessonRepository)
    {
        $this->repository= $lessonRepository;
    }

    public function insert($request){
        $data = $request;
        return $this->repository->create($data);
    }

    public function update($request, $id)
    {

        $result = $this->repository->update($request,$id);
        if ($result) {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $result = $this->repository->show($id);
        if ($result) {
            $this->repository->delete($id);
            return true;
        }
        return false;
    }

    public function getLessonsByCourse($id) {
        return $this->repository->getLessonsByCourse($id);
    }

    public function getCourseByLearn($id) {
        return $this->repository->getCourseByLearn($id);
    }

    public function getCourseID($id) {
        return $this->repository->getCourseID($id);
    }

    public function getFirstLessonByCourse($id) {
        return $this->repository->getFirstLessonByCourse($id);
    }
}
