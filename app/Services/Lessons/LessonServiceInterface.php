<?php

namespace App\Services\Lessons;

use App\Services\ServiceInterface;

interface LessonServiceInterface extends ServiceInterface
{
    public function getLessonsByCourse($id);

    public function getCourseByLearn($id);

    public function getCourseID($id);

    public function getFirstLessonByCourse($id);
}
