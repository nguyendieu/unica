<?php

namespace App\Services\Test;

use App\Services\ServiceInterface;

interface TestServiceInterface extends ServiceInterface
{
    public function getTestAndQuestion();

    public function getQuestionByTest($id);
}
