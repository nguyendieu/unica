<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryFormRequest;
use App\Repositories\Category\CategoryRepository;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{

    protected $serviceCategory;
    protected $categoryRepository;

    public function __construct(CategoryService    $categoryService,
                                CategoryRepository $categoryRepository)
    {
        $this->middleware('permission:categories-list', ['only' => ['index','show']]);
        $this->middleware('permission:categories-create', ['only' => ['create','store']]);
        $this->middleware('permission:categories-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:categories-delete', ['only' => ['destroy']]);
        $this->serviceCategory = $categoryService;
        $this->categoryRepository = $categoryRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.category.list', [
            'categories' => $this->categoryRepository->searchAndPaginate('name', $request->get('search'))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFormRequest $request)
    {

        $result = $this->categoryRepository->create($request->only('name', 'description', 'status'));
        if (!$result) {
            Session::flash('error', 'You are add new category fails');
            return redirect()->back();
        }
        Session::flash('success', 'You are add new category successfully');
        return redirect()->route('list_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        /dd($this->categoryReposiroty->show($id));
        return view('admin.category.show', [
            'category' => $this->categoryRepository->show($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->show($id);
        return view('admin.category.edit', [
            'category' => $category
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, $id)
    {
        $result = $this->categoryRepository->update($request->only('name', 'description', 'status'), $id);
        if ($result) {
            return redirect()->route('list_categories')->with('status', 'Update category successfully');
        }
        return redirect()->back()->with('status', 'update category fails');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->categoryRepository->show($id);
        if ($result) {
            $this->categoryRepository->delete($id);
            Session::flash('success', 'Delte Category Successfully');
            return redirect()->back();
        }
        Session::flash('error', 'Delte Category fail');
        return redirect()->back();
    }
}
