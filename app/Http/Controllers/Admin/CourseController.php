<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Courses\CourseFormRequest;
use App\Models\User;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Course\CourseRepository;
use App\Repositories\User\UserRepository;
use App\Services\Course\CourseServiceInterface;
use App\Services\Lessons\LessonServiceInterface;
use App\Services\User\UserServiceInterface;
use App\Utilities\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    protected $courseService;
    protected $categoryRepository;
    protected $userRepository;
    protected $lessonService;

    public function __construct(CourseServiceInterface $courseService,
                                CategoryRepository $categoryRepository,
                                UserRepository $userRepository,
                                LessonServiceInterface $lessonService
    )
    {
//        $this->middleware('permission:course-list', ['only' => ['index','show']]);
//        $this->middleware('permission:course-create', ['only' => ['create','store']]);
//        $this->middleware('permission:course-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:course-delete', ['only' => ['destroy']]);
        $this->courseService = $courseService;
        $this->categoryRepository = $categoryRepository;
        $this->userRepository = $userRepository;
        $this->lessonService = $lessonService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = $this->courseService->getCourseAndLessons('name', $request->get('search'));


        return view('admin.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categoryRepository->all();
        $users = $this->userRepository->all();

        return view('admin.course.create', compact('categories', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseFormRequest $request)
    {
        $data = $request->only('category_id', 'image', 'user_id','name','description','content','status');
        $data['status'] = $request->input('status') ? '1' : '0';

        if ($request->hasFile('image')) {
            $data['image'] = Common::uploadFile($request->file('image'), 'front/img/course_image');
        }

        $course = $this->courseService->create($data);

        return redirect('./admin/course/' . $course->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = $this->courseService->show($id);
        $lessons = $this->lessonService->getLessonsByCourse($id);

        return view('admin.course.show', compact('course', 'lessons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = $this->courseService->show($id);
        $users = $this->userRepository->all();
        $categories = $this->categoryRepository->all();

        return  view('admin.course.edit',compact('course','users','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('category_id', 'image', 'user_id','name','description','content','status');


        if ($request->hasFile('imgae')) {
            $data['image'] = Common::uploadFile($request->file('image'), 'front/img/course_image');

            $file_name_old = $request->get('image_old');
            if ($file_name_old != null) {
                unlink('front/img/course_image/' . $file_name_old);
            }
        }

        $this->courseService->update($data, $id);

        return redirect('admin/course/'. $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = $this->courseService->show($id);
        $this->courseService->delete($id);

        $file_name = $course->image;
        if ($file_name != null) {
            unlink('front/img/course_image/' . $file_name);
        }

        return redirect('admin/course');
    }
}
