<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Test\TestRepository;
use App\Services\Question\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class   QuestionController
{
    protected $questionService;
    protected $questionRepository;
    protected $testRepository;
    protected $answerRepository;


    public function __construct(
        QuestionService    $questionService,
        QuestionRepository $questionRepository,
        TestRepository     $testRepository,
        AnswerRepository   $answerRepository

    )
    {
        $this->questionService = $questionService;
        $this->questionRepository = $questionRepository;
        $this->testRepository = $testRepository;
        $this->answerRepository = $answerRepository;
    }

    public function index(Request $request)
    {

        $keyword = $request->key_word;
        $questions = $this->questionRepository->getQuestionByCondition($keyword);
        $answers = $this->answerRepository->all();
        return view('admin.question.index', compact('questions', 'answers'));
    }

    //show question
    public function show($id)
    {
        $question = $this->questionRepository->show($id);
        $answers = $this->questionService->showOptionQuestion($id);


        return view('admin.question.show', [
            'question' => $question,
            'answers' => $answers
        ]);
    }

    public function create()
    {
        $tests = $this->testRepository->all();
        return view('admin.question.create', compact('tests'));
    }

    public function store(Request $request)
    {

        if (!$this->testRepository->show($request->test_id))
            return back()->with('error', 'khong tim thay bai kiem tra');

        $new_question = [
            'question' => $request->question,
            'test_id' => $request->test_id,
        ];
        $question = $this->questionRepository->create($new_question);

        if ($question) {

            $answers = $request->answer;

            foreach ($answers as $k => $answer) {
                $new_answer = [
                    'question_id' => $question->id,
                    'answers' => $answer,
                    'correct' => 1 ? $k == $request->correct : 0
                ];

                $this->answerRepository->create($new_answer);
            }

            return Redirect()->route('admin.question.index')->with('yes', 'them moi thanh cong');
        }
        return Redirect()->back()->with('no', 'them moi that bai');
    }

    public function edit($id)
    {
        $questions = $this->questionRepository->show($id);
        $Answers = $this->questionService->showOptionQuestion($id);

//        $tests = $this->testRepository->all();
        $tests = $this->testRepository->show($questions->test_id);
//        dd($tests);
        return view('admin.question.edit', compact('questions', 'tests', 'Answers'));



//

    }

    public function update($id)
    {

        DB::transaction(function () {
            // dd(Request()->all());
            $id = Request()->id;
            $new_question = [
                'question' => Request()->question,
                'test_id' => Request()->test_id,
            ];

            $question = $this->questionRepository->update($new_question, $id);
            $idAnswers = $this->questionService->showOptionQuestion($id);
            //   dd($idAnswers);
            if ($idAnswers) {

                foreach (Request()->answer as $key => $answer) {
                    $new_answer = [
                        'answers' => $answer,
                        'correct' => 1 ? $key == Request()->correct : 0
                    ];
                    $answers = $this->answerRepository->update($new_answer, $idAnswers[$key]->id);

                }

            }

//            return view('admin.question.show', compact('question', 'answers'))->with('yes', 'sua thanh cong');

            return redirect()->route('admin.question.index',compact('question', 'answers'))->with('success','Update Question Successfully' );

        });
        return Redirect()->back()->with('no', 'sua that bai');
    }


    public function destroy($id)
    {
        $result = $this->questionRepository->delete($id);
        if ($result) {
            return redirect()->back()->with('yes', 'xóa thành công');
        }
        return redirect()->back()->with('yes', 'xóa thất  bại');
    }
}
