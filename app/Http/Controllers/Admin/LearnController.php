<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Services\Learn\LearnServiceInterface;
use App\Services\User\UserServiceInterface;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LearnController extends Controller
{
    protected $learnService;
    protected $courseRepository;
    protected $userService;

    public function __construct(LearnServiceInterface $learnService,CourseRepositoryInterface $courseRepository,UserServiceInterface $userService)
    {
        $this->learnService = $learnService;
        $this->courseRepository = $courseRepository;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = $this->courseRepository->getCourseByCondition();

        return view('admin.learn.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $course = $this->courseRepository->show($id);
        $users = $this->userService->all();


        return view('admin.learn.create',compact('course','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('course_id','student_id');
        $data['status'] = $request->input('status') ? '1' : '0';

        $this->learnService->create($data);

        return redirect('./admin/learn');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = $this->courseRepository->show($id);
        $numberStudent = $this->learnService->getStudentCount($id);
        $learns = $this->learnService->getUserName($id);

        return view('admin.learn.show',compact('course','learns','numberStudent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->learnService->delete($id);

        return back();
    }
}
