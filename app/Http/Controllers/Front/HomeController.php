<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\Course\CourseServiceInterface;
use App\Services\Learn\LearnServiceInterface;
use App\Services\Lessons\LessonServiceInterface;
use App\Services\Test\TestServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\Return_;

class HomeController extends Controller
{
    protected $courseService;
    protected $learnService;
    protected $lessonService;
    protected $testService;



    public function __construct(CourseServiceInterface $courseService,
                                LearnServiceInterface $learnService,
                                LessonServiceInterface $lessonService, TestServiceInterface $testService)
    {
        $this->courseService = $courseService;
        $this->learnService = $learnService;
        $this->lessonService = $lessonService;
        $this->testService = $testService;
    }

    public function index() {
        $coursesPaginate = $this->courseService->getCoursePaginate(3);
        $courses = $this->courseService->all();
        $studentCourses = $this->learnService->getStudentCourses(Auth::id());

        return view('front.index', compact('coursesPaginate','courses','studentCourses'));
    }

    public function show($id) {
        $studentCourse = $this->learnService->getStudentCourse(Auth::id());
        $studentCourses = $this->learnService->getStudentCourses(Auth::id());
        $course = $this->courseService->show($id);
        $lessons = $this->lessonService->getLessonsByCourse($id);
        $first_lesson = $this->lessonService->getFirstLessonByCourse($id);

        return view('front.show',compact('studentCourses','course','lessons','first_lesson','studentCourse'));
    }

    public function learn($id) {
        $studentCourses = $this->learnService->getStudentCourses(Auth::id());
        $courseID = $this->lessonService->getCourseID($id);
        $course = $this->lessonService->getCourseByLearn($id);
        foreach ($courseID as $item){
            $course_id = $item->course_id;
        }
        $lessons = $this->lessonService->getLessonsByCourse($course_id);
        $currentLearn = $id;
        $test = $this->testService->show($course_id);

        return view('front.learn',compact('course', 'lessons','studentCourses','currentLearn','test'));
    }
}
