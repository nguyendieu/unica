<?php

namespace App\Http\Requests\Admin\LessonRequest;

use Illuminate\Foundation\Http\FormRequest;

class LessonUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'type_lesson' => 'required',
            'content' => 'required',
            'video' => 'required',
            'course_id' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Tên  không được bỏ trống',
            'content.required' => 'Nội không được bỏ trống',
            'video.required' => 'Video không được bỏ trống',
            'course_id.required' => 'khóa học không tồn tại',
            'course_id.exists' => 'khóa học không tồn tại',
            'description.required' => 'Mô tả không được để trống',
            'type_lesson.required' => 'không được để trống'
        ];
    }
}
