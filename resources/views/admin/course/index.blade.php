@extends('admin.layouts.master')
@section('body')
    <!-- Main -->
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-ticket icon-gradient bg-mean-fruit"></i>
                    </div>
                    <div>
                        Courses
                        <div class="page-title-subheading">
                            View, create, update, delete and manage.
                        </div>
                    </div>
                </div>

                <div class="page-title-actions">
                    <a href="./admin/course/create" class="btn-shadow btn-hover-shine mr-3 btn btn-primary">
                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                        <i class="fa fa-plus fa-w-20"></i>
                                    </span>
                        Create
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">

                    <div class="card-header">

                        <form>
                            <div class="input-group">
                                <input type="search" name="search" id="search"
                                       placeholder="Search everything" class="form-control">
                                <span class="input-group-append">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i>&nbsp;
                                                    Search
                                                </button>
                                            </span>
                            </div>
                        </form>

                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <button class="btn btn-focus">This week</button>
                                <button class="active btn btn-focus">Anytime</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Author</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Lessons</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($courses as $course)
                                <tr>
                                    <td class="text-center text-muted">{{ $course->id }}</td>
                                    <td class="text-center">{{ $course->name }}</td>
                                    <td class="text-center">{{ $course->description }}</td>
                                    <td class="text-center">{{ $course->user->name }}</td>
                                    <td class="text-center">{{ $course->category->name }}</td>
                                    <td class="text-center">{{ count($course->lessons) }}</td>
                                    <td class="text-center">{{ $course->status = 1 ? "Show" : "Hide" }}</td>
                                    <td class="text-center">
                                        <a href="./admin/course/{{ $course->id }}/create"
                                           class="btn btn-hover-shine btn-success border-0 btn-sm">
                                            Add Lesson
                                        </a>
                                        <a href="./admin/ajax"
                                           class="btn btn-hover-shine btn-success border-0 btn-sm">
                                            comment
                                        </a>

                                        <a href="./admin/course/{{ $course->id }}"
                                           class="btn btn-hover-shine btn-outline-primary border-0 btn-sm">
                                            Details
                                        </a>
                                        <a href="./admin/course/{{ $course->id }}/edit" data-toggle="tooltip"
                                           title="Edit"
                                           data-placement="bottom" class="btn btn-outline-warning border-0 btn-sm">
                                                        <span class="btn-icon-wrapper opacity-8">
                                                            <i class="fa fa-edit fa-w-20"></i>
                                                        </span>
                                        </a>

                                        <form class="d-inline" action="./admin/course/{{ $course->id }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-hover-shine btn-outline-danger border-0 btn-sm"
                                                    type="submit" data-toggle="tooltip" title="Delete"
                                                    data-placement="bottom"
                                                    onclick="return confirm('Do you really want to delete this item?')">
                                                            <span class="btn-icon-wrapper opacity-8">
                                                                <i class="fa fa-trash fa-w-20"></i>
                                                            </span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>

                    <div class="d-block card-footer">
                        {{ $courses->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
@endsection
