@extends('admin.layouts.master')
@section('body')
    <!-- Main -->
    <div class="app-main__inner">

        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-ticket icon-gradient bg-mean-fruit"></i>
                    </div>
                    <div>
                        Learn
                        <div class="page-title-subheading">
                            View, create, update, delete and manage.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form method="post" action="./admin/learn/{{ $course->id }}/create" enctype="multipart/form-data">
                            @csrf
                            <div class="position-relative row form-group">
                                <input name="course_id" id="course_id" type="text"
                                       class="form-control" value="{{ $course->id }}" hidden>
                                <label for="course_id"
                                       class="col-md-3 text-md-right col-form-label">Course name</label>
                                <div class="col-md-9 col-xl-8">
                                    <p>{{ $course->name }}</p>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="student_id"
                                       class="col-md-3 text-md-right col-form-label">Student</label>
                                <div class="col-md-9 col-xl-8">
                                    <select required name="student_id" id="student_id"
                                            class="form-control">
                                        @foreach($users as $user)
                                            <option value={{ $user->id }}>
                                                {{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="status"
                                       class="col-md-3 text-md-right col-form-label">Status</label>
                                <div class="col-md-9 col-xl-8">
                                    <div class="position-relative form-check pt-sm-2">
                                        <input name="status" id="status" type="checkbox" value="1"
                                               class="form-check-input">
                                        <label for="status" class="form-check-label">Show</label>
                                    </div>
                                </div>
                            </div>

                            <div class="position-relative row form-group mb-1">
                                <div class="col-md-9 col-xl-8 offset-md-2">
                                    <a href="./admin/learn" class="border-0 btn btn-outline-danger mr-1">
                                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                                        <i class="fa fa-times fa-w-20"></i>
                                                    </span>
                                        <span>Cancel</span>
                                    </a>

                                    <button type="submit"
                                            class="btn-shadow btn-hover-shine btn btn-primary">
                                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                                        <i class="fa fa-download fa-w-20"></i>
                                                    </span>
                                        <span>Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
@endsection
