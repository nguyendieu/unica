@extends('admin.layouts.master')
@section('body')
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form method="post" action="" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="position-relative row form-group">
                                <label for="description"
                                       class="col-md-3 text-md-right col-form-label">test_name</label>
                                <div class="col-md-9 col-xl-8">
                                    <select required name="test_id" id="user_id"
                                            class="form-control">
                                        <option value={{ $tests->id }}>
                                            {{ $tests->title}}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">question </label>
                                <div class="col-md-9 col-xl-8">
                                    <textarea name="question" class="form-control"> {{$questions->question}}</textarea>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 1</label>
                                <div class="col-md-9 col-xl-8">
                                    <input name="answer[]" id="description" placeholder="option 1" type="text"
                                           class="form-control" value="{{$Answers[0]->answers}}" >
{{--                                    {{dd($Answers[1]->correct)}}--}}
                                    <input type="radio" value="0" name="correct" {{$Answers[0]->correct==1 ? 'checked' : '' }} >
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 2</label>
                                <div class="col-md-9 col-xl-8">
                                    <input name="answer[]" id="description" placeholder="option 2" type="text"
                                           class="form-control" value="{{$Answers[1]->answers}}">
                                    <input type="radio" name="correct" value="1" {{$Answers[1]->correct==1 ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 3</label>
                                <div class="col-md-9 col-xl-8">
                                    <input name="answer[]" id="description" placeholder="option 2" type="text"
                                           class="form-control" value="{{$Answers[2]->answers}}">
                                    <input type="radio" name="correct" value="2" {{$Answers[2]->correct==1 ? 'checked' : '' }}>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 4</label>
                                <div class="col-md-9 col-xl-8">
                                    <input name="answer[]" id="description" placeholder="option 4" type="text"
                                           class="form-control" value="{{$Answers[3]->answers}}">
                                    <input type="radio" name="correct" value="3" {{$Answers[3]->correct==1 ? 'checked' : '' }}>
                                </div>
                            </div>

                            <div class="position-relative row form-group mb-1">
                                <div class="col-md-9 col-xl-8 offset-md-2">
                                    <a href="#" class="border-0 btn btn-outline-danger mr-1">
                                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                                        <i class="fa fa-times fa-w-20"></i>
                                                    </span>
                                        <span>Cancel</span>
                                    </a>

                                    <button type="submit"
                                            class="btn-shadow btn-hover-shine btn btn-primary">
                                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                                        <i class="fa fa-download fa-w-20"></i>
                                                    </span>
                                        <span>Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
@endsection
