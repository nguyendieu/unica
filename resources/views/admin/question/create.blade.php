@extends('admin.layouts.master')
@section('body')
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form method="post" action="" enctype="multipart/form-data">
                            @csrf
                            <div class="position-relative row form-group">
                                <label for="description"
                                       class="col-md-3 text-md-right col-form-label">test_id</label>
                                <div class="col-md-9 col-xl-8">
                                    <select required name="test_id" id="user_id"
                                            class="form-control">
                                        <option value="">-- test_id --</option>
                                        @foreach($tests as $test)
                                            <option value={{ $test->id }}>
                                                {{ $test->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">Question </label>
                                <div class="col-md-9 col-xl-8">
                                    <textarea required name="question" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 1</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 1" type="text"
                                           class="form-control" value="">
                                    <input type="radio" value="0" name="correct">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 2</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 2" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="1">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 3</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 3" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="2">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 4</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 4" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="3">
                                </div>
                            </div>


                            <div class="position-relative row form-group mb-1">
                                <div class="col-md-9 col-xl-8 offset-md-2">
                                    <a href="#" class="border-0 btn btn-outline-danger mr-1">
                                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                                        <i class="fa fa-times fa-w-20"></i>
                                                    </span>
                                        <span>Cancel</span>
                                    </a>

                                    <button type="submit"
                                            class="btn-shadow btn-hover-shine btn btn-primary">
                                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                                        <i class="fa fa-download fa-w-20"></i>
                                                    </span>
                                        <span>Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
@endsection
